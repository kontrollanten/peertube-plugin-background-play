function register ({ registerHook, peertubeHelpers }) {
  const getProperties = () => {
    let hidden, visibilityChange;

    if (typeof document.hidden !== 'undefined') {
      hidden = 'hidden';
      visibilityChange = 'visibilitychange';
    } else if (typeof document.msHidden !== 'undefined') {
      hidden = 'msHidden';
      visibilityChange = 'msvisibilitychange';
    } else if (typeof document.webkitHidden !== 'undefined') {
      hidden = 'webkitHidden';
      visibilityChange = 'webkitvisibilitychange';
    }

    return { hidden, visibilityChange };
  };

  const { hidden, visibilityChange } = getProperties();

  if (typeof document.addEventListener === 'undefined' || hidden === undefined) {
    return;
  }

  const videoState = {
    hasPlayed: false,
    pausedAt: null,
  };

  const handleVideoLoaded = () => {
    const videoElement = document.querySelector('video');

    videoState.hasPlayed = videoElement.currentTime > 0 && videoElement.paused === false && videoElement.ended === false;

    videoElement.addEventListener('play', () => {
      videoState.hasPlayed = true;
    });

    videoElement.addEventListener('pause', () => {
      videoState.pausedAt = new Date();
    });
  };

  registerHook({
    target: 'action:video-watch.player.loaded',
    handler: handleVideoLoaded,
  });

  const handleVisibilityChange = () => {
    if (!document[hidden]) {
      console.info('Document isn\'t hidden, exiting.');
      return;
    }

    if (videoState.hasPlayed === false) {
      console.info('Video haven\'t been playing, therefore won\'t start it.');
      return;
    }

    const now = new Date().getTime();
    if (videoState.pausedAt !== null && (now - videoState.pausedAt.getTime()) > 1000) {
      console.info('Video seems to have been paused by the user, therefore won\'t start it.');
      return;
    }

    const videoElement = document.querySelector('video');

    if (!videoElement) {
      console.info('No video element found, exiting.');
      return;
    }

    if (videoElement.paused) {
      videoElement.play();
    } else {
      // Chrome, Android
      setTimeout(() => {
        videoElement.play();
      });
    }
  }

  document.addEventListener(visibilityChange, handleVisibilityChange, false);
}

export {
  register
}
